/**
 * Задача 1.
 *
 * Создайте функцию `f`, которая возвращает куб числа, переданного в качестве аргумента.
 *
 * Условия:
 * - Генерировать ошибку, если в качестве аргумента был передан не числовой тип.
 */

// РЕШЕНИЕ

function f(num) {
    return (typeof num !== "number"? console.log("Arg is not a number!"):Math.pow(num, 3)) 

    // if(typeof num !== "number") {
    //     console.log("Arg is not a number!");
    // }
    // else {
    //     return Math.pow(num, 3);
    // }
}

console.log(f(2)); // 8
