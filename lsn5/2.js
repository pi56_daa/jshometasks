/**
 * Задача 2.
 *
 * Создайте функцию `f`, которая возвращает сумму всех переданных числовых аргументов.
 *
 * Условия:
 * - Функция должна принимать любое количество аргументов;
 * - Генерировать ошибку, если в качестве любого входного аргумента было предано не число.
 */

// РЕШЕНИЕ

function f(...args) {
    let result = 0;
    for (const element of args) {
        if(typeof element !== 'number'){
            throw TypeError('One of args is not a number')
        }
        result += element;
    }
  return result;
}

console.log(f(1, 1, 1, 2, 1, 1, 1, 1)); // 9
