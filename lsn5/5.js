/**
 * Задача 3.
 *
 * Создайте функцию createFibonacciGenerator().
 *
 * Вызов функции createFibonacciGenerator() должен возвращать объект, который содержит два метода:
 * - print — возвращает число из последовательности Фибоначчи;
 * - reset — обнуляет последовательность и ничего не возвращает.
 *
 * Условия:
 * - Задачу нужно решить с помощью замыкания.
 */

// РЕШЕНИЕ



function createFibonacciGenerator(){
    let arr = [0, 1];

    return{
     print:function(){
       
        let num = arr[arr.length - 1],
        len = arr.length;
        arr.push(arr[len - 1] + arr[len - 2]);
        return num;
    },
    reset:function(){
       
        arr = [0, 1];
        return undefined;

    }
}

}




const generator1 = createFibonacciGenerator();


console.log(generator1.print()); // 1
console.log(generator1.print()); // 1
console.log(generator1.print()); // 2
console.log(generator1.print()); // 3
console.log(generator1.reset()); // undefined
console.log(generator1.print()); // 1
console.log(generator1.print()); // 1
console.log(generator1.print()); // 2

const generator2 = createFibonacciGenerator();

console.log(generator2.print()); // 1
console.log(generator2.print()); // 1
console.log(generator2.print()); // 2

exports.createFibonacciGenerator = createFibonacciGenerator;
