/**
 * Задача 1.
 *
 * Создайте свойство `salary` в объекте `person`.
 * При чтении этого свойства должна возвращаться строка с текстом.
 * Возвращаемая стрка должна содержать текст: `Зарплата за проект составляет 100$`
 * Где 100 это произведение ставки в час `rate` на количество отработанных часов `hours`
 * 
 * Создайте свойство `rate` в объекте `person`.
 * Свойство `rate` можно менять, но нельзя удалять.
 * Свойство `rate` должно содержать число.
 * 
 * Создайте свойство `hours` в объекте `person`.
 * Свойство `hours` можно менять, но нельзя удалять.
 * Свойство `hours` должно содержать число.
 * 
 * Условия:
 * - Свойство salary обязательно должно быть геттером.
 * 
 * Обратите внимание!
 * - Для того что бы обратиться к свойству оъекта необходимо использовать this.hours и this.rate
 * - Для решения данного задания вам потребуется defineProperty или defineProperties
 */

let hours = 1;
let rate = 1;
let summ = false;
const person = {
    get salary() {
        return `Зарплата за проект составляет ${summ}`
    },
};

// РЕШЕНИЕ

//const object1 = {};

Object.defineProperty(person, 'rate', {
  writable: true,
  configurable: false,
});
person.rate = 20;

Object.defineProperty(person, 'hours', {
    writable: true,
    configurable: false,
})
person.hours = 5;

summ = person.hours*person.rate


console.log(summ)


console.log(person.salary); // `Зарплата за проект составляет ххх$`
