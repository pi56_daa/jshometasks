/**
 * Задача 5.
 *
 * Напишите функцию `createArray`, которая будет создавать массив с заданными значениями.
 * Первым параметром функция принимает значение, которым заполнять массив.
 * А вторым — количество элементов, которое должно быть в массиве.
 *
 * Генерировать ошибки, если:
 * - В качестве первого аргумента были переданы не число, не строка, не объект и не массив;
 * - В качестве второго аргумента был передан не число.
 */

// Решение
function createArray(newValue, elements) {
    const type = typeof newValue;
    const typeArray = ['object', 'number', 'string']
    if(!typeArray.includes(type))
    throw new TypeError('Type of first arg is not correct')
    if(typeof elements !=='number')
    throw new TypeError('Type of second arg is not number')
    let result = new Array(elements);
    return result.fill(newValue)
}
//return new Array(elements).fill(newValue)
console.log(createArray('x', 5)); // [ x, x, x, x, x ]

exports.createArray = createArray;
