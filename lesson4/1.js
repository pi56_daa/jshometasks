/**
 * Задача 1.
 *
 * Вручную создать имплементацию функции `filter`.
 * Логика работы ручной имплементации должна быть такой-же,
 * как и у встроенного метода.
 *
 * Заметки:
 * - Встроенный метод Array.prototype.filter использовать запрещено.
 *
 * Генерировать ошибки, если:
 * - В качестве первого аргумента был передан не массив;
 * - В качестве второго аргумента была передана не функция.
 *
 * Заметки:
 * - Второй аргумент встроенного метода filter (thisArg) имплементировать не нужно.
 */

const array = ['Доброе утро!', 'Добрый вечер!', 3, 512, '#', 'До свидания!'];

const arrLength = array.length;
const newArr = [];

for(let i = 0; i < arrLength; i++) {
    if(array[i] === 'Добрый вечер!') {
        newArr[i] = array[i];
        console.log(newArr[i])
    }
}

// Решение

// const filteredArray = filter(array, (element, index, arrayRef) => {
//     console.log(`${index}:`, element, arrayRef);

//     return element === 'Добрый вечер!';
// });

//  console.log(filteredArray); // ['Добрый вечер!']