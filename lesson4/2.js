/**
 * Задача 2.
 *
 * Вручную создать имплементацию функции `reduce`.
 * Логика работы ручной имплементации должна быть такой-же,
 * как и у встроенного метода.
 *
 * Заметки:
 * - Встроенные методы Array.prototype.reduce и Array.prototype.reduceRight использовать запрещено;
 * - Третий аргумент функции reduce является не обязательным;
 * - Если третий аргумент передан — он станет начальным значением аккумулятора;
 * - Если третий аргумент не передан — начальным значением аккумулятора станет первый элемент обрабатываемого массива.
 *
 * Генерировать ошибки, если:
 * - В качестве первого аргумента был передан не массив;
 * - В качестве второго аргумента была передана не функция;
 */

const array = [1, 2, 3, 4, 5];
const INITIAL_ACCUMULATOR = 6;
let summa = 0;
// Решение
for(var i = 0; i<array.length; i++) {
    summa = summa + array[i];
    console.log(`${i}: `, array[i], summa);
}
let someRes = summa + INITIAL_ACCUMULATOR;
console.log(someRes);

// const result = array.reduce(
//     (accumulator, element, index) => {
//         console.log(`${index}:`, element, accumulator);
//         return accumulator + element;
//     },
//     INITIAL_ACCUMULATOR,
// );

// console.log(result); // 21